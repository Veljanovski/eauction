﻿using eAuction.Models;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;

namespace eAuction.Helpers
{
    public class Helper
    {
        public RedisClient redisClient = new RedisClient("localhost");

        private void HSET(string hash, Item item)
        {
            redisClient.SetEntryInHash(hash, "Name", item.Name);
            redisClient.SetEntryInHash(hash, "Estimate", item.Estimate.ToString());
            redisClient.SetEntryInHash(hash, "Current", item.Current.ToString());
            redisClient.SetEntryInHash(hash, "Description", item.Description);
            redisClient.SetEntryInHash(hash, "Category", item.Category);
            redisClient.SetEntryInHash(hash, "Sale_End", item.Sale_End.ToString());
        }

        private Item HGET(string hash)
        {
            Item item = new Item();
            item.Name = redisClient.GetValueFromHash(hash, "Name");
            item.Estimate = Convert.ToInt32(redisClient.GetValueFromHash(hash, "Estimate"));
            item.Current = Convert.ToInt32(redisClient.GetValueFromHash(hash, "Current"));
            item.Description = redisClient.GetValueFromHash(hash, "Description");
            item.Category = redisClient.GetValueFromHash(hash, "Category");
            item.Sale_End = Convert.ToDateTime(redisClient.GetValueFromHash(hash, "Sale_End"));

            return item;
        }

        private void HDEL(string hash)
        {
            redisClient.RemoveEntryFromHash(hash, "Name");
            redisClient.RemoveEntryFromHash(hash, "Estimate");
            redisClient.RemoveEntryFromHash(hash, "Current");
            redisClient.RemoveEntryFromHash(hash, "Description");
            redisClient.RemoveEntryFromHash(hash, "Category");
            redisClient.RemoveEntryFromHash(hash, "Sale_End");
        }

        private List<Item> DummyData()
        {
            Item item1 = new Item();
            item1.Name = "Chopard Watch";
            item1.Estimate = 8000;
            item1.Current = 5000;
            item1.Description = @"This is an exceptional, fine quality gentleman's 18k rose gold weighty automatic Chopard Classic wristwatch with date window and original leather strap.
                                The case and buckle of this watch are 18ct rose gold(case hallmarked, buckle stamped 750).";
            item1.Category = "Watches & Jewellery";
            item1.Sale_End = Convert.ToDateTime("31-Jan-21 5:05:16 PM");

            Item item2 = new Item();
            item2.Name = "Ankarlak Chain";
            item2.Estimate = 3000;
            item2.Current = 1800;
            item2.Description = @"Length approx. 48 cm. Use Wear.";
            item2.Category = "Watches & Jewellery";
            item2.Sale_End = Convert.ToDateTime("26-Jan-21 7:15:16 PM");

            Item item3 = new Item();
            item3.Name = "Christian Voulethe";
            item3.Estimate = 150;
            item3.Current = 250;
            item3.Description = @"Oil on canvas, 73x60 cm. Christian Voulethe (1922-1989) is such an occasion.";
            item3.Category = "Fine Art";
            item3.Sale_End = Convert.ToDateTime("28-Jan-21 1:15:16 PM");

            Item item4 = new Item();
            item4.Name = "Lars Lerin";
            item4.Estimate = 9000;
            item4.Current = 4500;
            item4.Description = @"A terego titled: Deception island, Antarctica, Lars Lerin 41x62.5 cm, frame size 56.5x78.5 cm.";
            item4.Category = "Fine Art";
            item4.Sale_End = Convert.ToDateTime("25-Jan-21 1:55:22 PM");
            
            Item item5 = new Item();
            item5.Name = "Greta Garbos Armchair";
            item5.Estimate = 900;
            item5.Current = 350;
            item5.Description = @"Beech wood. Back and seat of rattan. Carved decoration depicting flowers, leaves and scrolls. Slightly curved legs. Green-clad cushion.";
            item5.Category = "Furniture";
            item5.Sale_End = Convert.ToDateTime("28-Jan-21 5:55:22 PM");

            Item item6 = new Item();
            item6.Name = "Gustaf Foltiern";
            item6.Estimate = 2000;
            item6.Current = 900;
            item6.Description = @"Veneered with, among other things, walnut, mahogany and elm. Gold-plated brass fittings. Slice of red limestone. Behind flap compartments and small drawers.";
            item6.Category = "Furniture";
            item6.Sale_End = Convert.ToDateTime("30-Jan-21 4:00:00 PM");

            Item item7 = new Item();
            item7.Name = "Litza Bavarian";
            item7.Estimate = 150;
            item7.Current = 100;
            item7.Description = @"Size 40. Stain Spot at right cuff.";
            item7.Category = "Vintage Fashion";
            item7.Sale_End = Convert.ToDateTime("25-Jan-21 5:00:00 PM");

            Item item8 = new Item();
            item8.Name = "Fur Coat";
            item8.Estimate = 800;
            item8.Current = 190;
            item8.Description = @"Short mink fur with three-quarter sleeves, decorative buttons - husky buttoning, insert pockets. Estimated size 36.";
            item8.Category = "Vintage Fashion";
            item8.Sale_End = Convert.ToDateTime("29-Jan-21 3:00:00 PM");

            List<Item> list = new List<Item>();
            list.Add(item1); list.Add(item2); list.Add(item3); list.Add(item4);
            list.Add(item5); list.Add(item6); list.Add(item7); list.Add(item8);

            return list;
        }

        public void InitialStartUp()
        {
            var exists = redisClient.GetListCount("itemIdList");
            if (exists != 0)
                return;

            for (int j = 1; j < 9; j++)
            {
                redisClient.AddItemToList("itemIdList", "itemhash:" + j);
            }

            List<Item> list = DummyData();

            var items = redisClient.GetAllItemsFromList("itemIdList");

            for (int j = 0; j < items.Count; j++) 
            {
                HSET(items[j], list[j]);
                redisClient.AddItemToSet("Category:" + list[j].Category, (j + 1).ToString());
            }
        }

        public List<(Item Item, int Id)> FetchAll()
        {
            List<(Item Item, int Id)> tuple = new List<(Item Item, int Id)>();

            var count = redisClient.GetListCount("itemIdList");

            for (int i = 0; i < count; i++)
            {
                string hash = redisClient.GetItemFromList("itemIdList", i);
                Item item = HGET(hash);

                int id = Convert.ToInt32(hash.Split(":")[1]);

                tuple.Add((item, id));
            }

            return tuple;
        }

        public List<(Item item, int Id)> FetchCategory(string category)
        {

            var setIds = redisClient.GetAllItemsFromSet("Category:" + category);
            var idsList = setIds.ToList();

            List<(Item Item, int Id)> tuple = new List<(Item Item, int Id)>();

            foreach (string id in idsList)
            {
                Item item = HGET("itemhash:" + id);
                tuple.Add((item, Convert.ToInt32(id)));
            }

            return tuple;
        }

        public Item FetchItem(int id)
        {
            string hash = "itemhash:" + id;
            return HGET(hash);
        }

        public void AddNew(Item item)
        {
            var count = (int)redisClient.GetListCount("itemIdList");
            string hash = redisClient.GetItemFromList("itemIdList", count - 1);
            int id = Convert.ToInt32(hash.Split(":")[1]);

            id += 1;

            hash = "itemhash:" + id;
            redisClient.AddItemToList("itemIdList", hash);
            redisClient.AddItemToSet("Category:" + item.Category, id.ToString());
            HSET(hash, item);
        }

        public void EditItem(Item item, int Id)
        {
            string hash = "itemhash:" + Id;
            HSET(hash, item);
        }

        public void PlaceBid(int Id, int NewBid)
        {
            redisClient.SetEntryInHash("itemhash:" + Id, "Current", NewBid.ToString());
        }

        public void DeleteItem(int Id)
        {
            string hash = "itemhash:" + Id;
            Item item = FetchItem(Id);

            HDEL(hash);
            redisClient.RemoveItemFromList("itemIdList", hash);
            redisClient.RemoveItemFromSet("Category:" + item.Category, Id.ToString());
        }
        
    }
}
