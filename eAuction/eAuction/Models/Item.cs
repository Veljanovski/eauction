﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eAuction.Models
{
    public class Item
    {
        public string Name { get; set; }
        public int Estimate { get; set; }
        public int Current { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public DateTime Sale_End { get; set; }
    }
}
