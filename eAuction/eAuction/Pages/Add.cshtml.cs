using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eAuction.Helpers;
using eAuction.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace eAuction.Pages
{
    public class AddModel : PageModel
    {
        [BindProperty]
        public Item Item { get; set; }
        public Helper Helper { get; set; }

        public AddModel()
        {
            Helper = new Helper();
        }

        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            if (!this.ModelState.IsValid)
            {
                return this.Page();
            }

            Helper.AddNew(Item);

            return RedirectToPage("Add");
        }
    }
}
