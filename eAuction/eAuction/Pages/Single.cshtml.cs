using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eAuction.Helpers;
using eAuction.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace eAuction.Pages
{
    public class SingleModel : PageModel
    {
        Helper Helper { get; set; }
        [BindProperty]
        public Item Item { get; set; }
        [BindProperty]
        public int Id { get; set; }
        [BindProperty]
        public int NewBid { get; set; }
        public IList<(Item Item, int Id)> items { get; set; }

        public SingleModel()
        {
            Helper = new Helper();
        }

        public void OnGet(int id)
        {
            Id = id;
            Item = Helper.FetchItem(id);

            items = Helper.FetchAll();

            var index = -1;
            for (int i = 0; i < items.Count; i++)
                if (items[i].Id == Id)
                {
                    index = i;
                    break;
                }

            items.RemoveAt(index);
        }

        public IActionResult OnPostBid()
        {
            if (!this.ModelState.IsValid)
            {
                return this.Page();
            }

            if (NewBid > Item.Current)
                Helper.PlaceBid(Id, NewBid);

            return Redirect("Single?id=" + Id);
        }

        public IActionResult OnPostDelete()
        {
            if (!this.ModelState.IsValid)
            {
                return this.Page();
            }

            Helper.DeleteItem(Id);

            return RedirectToPage("Index");
        }
    }
}
