﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eAuction.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using ServiceStack.Redis;
using Newtonsoft.Json;
using System.Text;
using eAuction.Helpers;

namespace eAuction.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        Helper Helper { get; set; }
        public IList<(Item Item, int Id)> items { get; set; }

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;

            Helper = new Helper();
            Helper.InitialStartUp();
        }

        public void OnGet()
        {
            items = Helper.FetchAll();
        }

        public void OnGetCategory(string category)
        {
            items = Helper.FetchCategory(category);
        }
    }
}
