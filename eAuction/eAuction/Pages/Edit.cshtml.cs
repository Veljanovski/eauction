using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using eAuction.Helpers;
using eAuction.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace eAuction.Pages
{
    public class EditModel : PageModel
    {

        [BindProperty]
        public Item Item { get; set; }
        public Helper Helper { get; set; }
        [BindProperty]
        public int Id { get; set; }

        public EditModel()
        {
            Helper = new Helper();
        }

        public void OnGet(int id)
        {
            Id = id;
            Item = Helper.FetchItem(id);
        }
        public IActionResult OnPost()
        {
            if (!this.ModelState.IsValid)
            {
                return this.Page();
            }

            Helper.EditItem(Item, Id);

            return Redirect("Single?id=" + Id);
        }
    }
}
